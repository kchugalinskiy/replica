package main

import (
	"fmt"
	"sync"
)

const (
	messageBufSize = 10
)

type State struct {
	data []int32
	m    sync.RWMutex
	chm  sync.RWMutex
	ch   chan *Diff
}

func NewState() *State {
	return &State{
		data: make([]int32, 0),
	}
}

func (s *State) InitState(d []int32) {
	s.m.Lock()
	defer s.m.Unlock()

	s.data = d
}

func (s *State) Start() {
	s.chm.Lock()
	defer s.chm.Unlock()

	s.ch = make(chan *Diff, messageBufSize)

	go func() {
		for {
			d, ok := <-s.ch
			if !ok {
				// TODO: log processing stopped event
				// fmt.Printf("processing stopped")
				return
			}

			s.applyImpl(d)
		}
	}()
}

func (s *State) Stop() {
	s.chm.Lock()
	defer s.chm.Unlock()

	if s.ch != nil {
		close(s.ch)
		s.ch = nil
	}
}

func (s *State) Apply(d *Diff) {
	s.chm.RLock()
	defer s.chm.RUnlock()

	if s.ch != nil {
		s.ch <- d
	}
}

func (s *State) applyImpl(d *Diff) {
	s.m.Lock()
	defer s.m.Unlock()

	switch d.Operation {
	case DiffOperationCreate:
		s.create(d.Value)
	case DiffOperationUpdate:
		s.update(d.Value, d.Index)
	case DiffOperationDelete:
		s.delete(d.Index)
	default:
		panic(fmt.Sprintf("unsupported operation %v", d.Operation))
	}
}

func (s *State) create(v int32) {
	place := firstGreaterOrEqual(s.data, v)
	if place >= len(s.data) {
		s.data = append(s.data, v)
		return
	}

	if place == 0 {
		s.data = append([]int32{v}, s.data...)
		return
	}

	s.data = append(s.data, v)
	for i := len(s.data) - 1; i >= place+1; i-- {
		s.data[i] = s.data[i-1]
	}
	s.data[place] = v
}

func (s *State) update(v int32, i int) {
	if i < 0 || i >= len(s.data) {
		// TODO: process index out of bounds error
		// fmt.Printf("either panic or log: index %d out of range [0,%d)\n", i, len(s.data))
		return
	}

	s.delete(i)
	s.create(v)
}

func (s *State) delete(i int) {
	if i < 0 || i >= len(s.data) {
		// TODO: process index out of bounds error
		// fmt.Printf("either panic or log: index %d out of range [0,%d)\n", i, len(s.data))
		return
	}
	if i == 0 {
		s.data = s.data[i+1:]
		return
	}

	if i == len(s.data)-1 {
		s.data = s.data[0:i]
		return
	}

	s.data = append(s.data[0:i], s.data[i+1:len(s.data)]...)
}

func firstGreaterOrEqual(data []int32, v int32) int {
	if len(data) == 0 {
		return 0
	}
	if len(data) == 1 {
		if data[0] >= v {
			return 0
		}
		return 1
	}
	half := len(data) / 2
	if data[half] >= v {
		return firstGreaterOrEqual(data[0:half], v)
	}
	return half + firstGreaterOrEqual(data[half:len(data)], v)
}

func (s *State) Get(i int) (int32, bool) {
	s.m.RLock()
	defer s.m.RUnlock()

	if i < 0 || i >= len(s.data) {
		return 0, false
	}

	return s.data[i], true
}

func (s *State) Len() int {
	s.m.RLock()
	defer s.m.RUnlock()

	return len(s.data)
}

func (s *State) Clone() *State {
	s.m.RLock()
	defer s.m.RUnlock()

	sn := &State{
		data: make([]int32, len(s.data)),
	}
	copy(sn.data, s.data)
	return sn
}
