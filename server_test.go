package main

import (
	"math/rand"
	"reflect"
	"sync"
	"testing"
	"time"
)

const (
	initialCount = 10000000 // initial dataset size
	clientsCount = 100      // your requirement was 20 concurrent clients, I've increased your limit
	opsCount     = 5        // I've volunteered to increase operations from 5 RPS up to 100 in a batch
	maxInt       = int32(1000000000)
	syncDelay    = 50 * time.Millisecond // your requirement was 1second btw
)

var initBuf []int32

func init() {
	init := make([]int32, initialCount)
	for i := 0; i != initialCount; i++ {
		init = append(init, rand.Int31n(maxInt))
	}
}

func BenchmarkRequirements(b *testing.B) {
	for i := 0; i < b.N; i++ {
		func() {
			srv := NewServer()
			srv.Start()
			defer srv.Close()
			srv.InitState(initBuf)

			cli1 := NewClient()
			defer cli1.Close()

			// simulate client-server interaction
			cli1.Connect(srv)
			srv.Accept(cli1)
			defer srv.Disconnect(cli1)

			var wg sync.WaitGroup
			wg.Add(clientsCount)
			for i := 0; i != clientsCount; i++ {
				go func() {
					defer wg.Done()

					cli := NewClient()
					defer cli.Close()

					// simulate client-server interaction
					cli.Connect(srv)
					srv.Accept(cli)
					defer srv.Disconnect(cli)

					for op := 0; op != opsCount*b.N; op++ {
						// do some crazy random actions
						switch rand.Intn(3) {
						case 0:
							cli.Create(rand.Int31() % maxInt)
						case 1:
							cli.Update(rand.Int31()%maxInt, rand.Intn(cli.Len()+1))
						case 2:
							cli.Delete(rand.Intn(cli.Len() + 1))
						}
					}
				}()
			}

			// wait for clients to finish all image send operations
			wg.Wait()
			// once sending is done, wait for the server to process all operations (if you read this, your requirement was 1s ;-) )
			time.Sleep(syncDelay)

			cli2 := NewClient()
			defer cli2.Close()

			// simulate client-server interaction
			cli2.Connect(srv)
			srv.Accept(cli2)
			defer srv.Disconnect(cli2)

			// sync of the new client is done, check the result
			if !reflect.DeepEqual(srv.State.data, cli1.State.data) {
				b.Fatalf("check 1 has failed")
			}
			if !reflect.DeepEqual(srv.State.data, cli2.State.data) {
				b.Fatalf("check 2 has failed")
			}
		}()
	}
}
