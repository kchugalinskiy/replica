package main

type DiffOperation int

const (
	DiffOperationNone = iota
	DiffOperationCreate
	DiffOperationUpdate
	DiffOperationDelete
)

type Diff struct {
	Operation DiffOperation
	Index     int
	Value     int32
}
