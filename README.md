## TL;DR

Code is not perfect, badly documented and performance may be enhanced more

## Usage

```go
// create server and start listening for incoming connections
srv := NewServer()
srv.Start()
defer srv.Close()
// init server state with some data
var initBuf []int32
srv.InitState(initBuf)
			
// create and initialize the client
cli := NewClient()
defer cli.Close()

// connect to server (simplification, this step is done through some RPC calls)
cli1.Connect(srv)
srv.Accept(cli)
defer srv.Disconnect(cli)

// do some operations, which are being sent to the server
cli.Create(1)
cli.Update(2, 0)
cli.Delete(0)

// wait for the client to sync with "remote" state
time.Sleep(50*time.Millisecond)

// get local state
val, found := cli.Get(0)
if !found {
	// ..
}
```

## Implementation details

All commands applied to the client are just sent to the server. Server queues them using golang channel and on each command apply it notifies all connected clients about this change. Clients are applying only those commands which have been received from a remote server. 

Once the communication is done, it takes some (short) time to get pending changes from the server, the subscription model allows to reduce its limit

## Performance testing

If you launch golang benchmark, you'll get the amount of 5-batch operations run by a 100 clients concurrently on an initial array of 10mln items with 50ms sync delay

On my PC I've got

```
goos: linux
goarch: amd64
pkg: gitlab.com/kchugalinskiy/replica
BenchmarkRequirements
BenchmarkRequirements-12    	      20	 337056575 ns/op
PASS
```

## Extra part

To allow offline client mode all pending operations being sent to the client should be applied to the local state immediately, but taking into consideration that server commands have priority and would overwrite locally applied ones