package main

import (
	"reflect"
	"testing"
)

func Test_update(t *testing.T) {
	data := []struct {
		Data     []int32
		Index    int
		Value    int32
		Expected []int32
	}{
		{
			Data:     []int32{},
			Index:    1,
			Value:    1,
			Expected: []int32{},
		},
		{
			Data:     []int32{},
			Index:    -1,
			Value:    1,
			Expected: []int32{},
		},
		{
			Data:     []int32{2},
			Index:    0,
			Value:    1,
			Expected: []int32{1},
		},
		{
			Data:     []int32{1, 3, 5, 7},
			Index:    0,
			Value:    0,
			Expected: []int32{0, 3, 5, 7},
		},
		{
			Data:     []int32{1, 3, 5, 7},
			Index:    1,
			Value:    2,
			Expected: []int32{1, 2, 5, 7},
		},
		{
			Data:     []int32{1, 3, 5, 7},
			Index:    2,
			Value:    4,
			Expected: []int32{1, 3, 4, 7},
		},
		{
			Data:     []int32{1, 3, 5, 7},
			Index:    3,
			Value:    6,
			Expected: []int32{1, 3, 5, 6},
		},

		{
			Data:     []int32{1, 3, 5, 7},
			Index:    3,
			Value:    0,
			Expected: []int32{0, 1, 3, 5},
		},
		{
			Data:     []int32{1, 3, 5, 7},
			Index:    2,
			Value:    2,
			Expected: []int32{1, 2, 3, 7},
		},
		{
			Data:     []int32{1, 3, 5, 7},
			Index:    1,
			Value:    4,
			Expected: []int32{1, 4, 5, 7},
		},
		{
			Data:     []int32{1, 3, 5, 7},
			Index:    0,
			Value:    6,
			Expected: []int32{3, 5, 6, 7},
		},
		{
			Data:     []int32{1, 3, 5, 7},
			Index:    0,
			Value:    8,
			Expected: []int32{3, 5, 7, 8},
		},
	}

	for i, d := range data {
		s := NewState()
		s.data = make([]int32, len(d.Data))
		copy(s.data, d.Data)

		s.update(d.Value, d.Index)
		if !reflect.DeepEqual(s.data, d.Expected) {
			t.Fatalf("test %d: updating index %d with %d data %+v expected %+v", i, d.Index, d.Value, s.data, d.Expected)
		}
	}
}

func Test_create(t *testing.T) {
	data := []struct {
		Data     []int32
		Value    int32
		Expected []int32
	}{
		{
			Data:     []int32{},
			Value:    1,
			Expected: []int32{1},
		},
		{
			Data:     []int32{2},
			Value:    1,
			Expected: []int32{1, 2},
		},
		{
			Data:     []int32{2},
			Value:    3,
			Expected: []int32{2, 3},
		},
		{
			Data:     []int32{1, 3, 5, 7, 9},
			Value:    0,
			Expected: []int32{0, 1, 3, 5, 7, 9},
		},
		{
			Data:     []int32{1, 3, 5, 7, 9},
			Value:    2,
			Expected: []int32{1, 2, 3, 5, 7, 9},
		},
		{
			Data:     []int32{1, 3, 5, 7},
			Value:    4,
			Expected: []int32{1, 3, 4, 5, 7},
		},
		{
			Data:     []int32{1, 3, 5, 7},
			Value:    6,
			Expected: []int32{1, 3, 5, 6, 7},
		},
		{
			Data:     []int32{1, 3, 5, 7},
			Value:    8,
			Expected: []int32{1, 3, 5, 7, 8},
		},
	}

	for i, d := range data {
		s := NewState()
		s.data = make([]int32, len(d.Data))
		copy(s.data, d.Data)

		s.create(d.Value)
		if !reflect.DeepEqual(s.data, d.Expected) {
			t.Fatalf("case %d: inserting %d data %+v expected %+v", i, d.Value, s.data, d.Expected)
		}
	}
}

func Test_delete(t *testing.T) {
	data := []struct {
		Data     []int32
		Index    int
		Expected []int32
	}{
		{
			Data:     []int32{},
			Index:    0,
			Expected: []int32{},
		},
		{
			Data:     []int32{},
			Index:    -1,
			Expected: []int32{},
		},
		{
			Data:     []int32{2},
			Index:    0,
			Expected: []int32{},
		},
		{
			Data:     []int32{1, 3, 5, 7, 9},
			Index:    0,
			Expected: []int32{3, 5, 7, 9},
		},
		{
			Data:     []int32{1, 3, 5, 7, 9},
			Index:    1,
			Expected: []int32{1, 5, 7, 9},
		},
		{
			Data:     []int32{1, 3, 5, 7, 9},
			Index:    2,
			Expected: []int32{1, 3, 7, 9},
		},
		{
			Data:     []int32{1, 3, 5, 7, 9},
			Index:    3,
			Expected: []int32{1, 3, 5, 9},
		},
		{
			Data:     []int32{1, 3, 5, 7, 9},
			Index:    4,
			Expected: []int32{1, 3, 5, 7},
		},
	}

	for i, d := range data {
		s := NewState()
		s.data = make([]int32, len(d.Data))
		copy(s.data, d.Data)

		s.delete(d.Index)
		if !reflect.DeepEqual(s.data, d.Expected) {
			t.Fatalf("case %d: deleting %d data %+v expected %+v", i, d.Index, s.data, d.Expected)
		}
	}
}

func Test_binSearch(t *testing.T) {
	data := []struct {
		Data     []int32
		Value    int32
		Expected int
	}{
		{
			Data:     []int32{},
			Value:    13,
			Expected: 0,
		},
		{
			Data:     []int32{0},
			Value:    1,
			Expected: 1,
		},
		{
			Data:     []int32{2},
			Value:    1,
			Expected: 0,
		},
		{
			Data:     []int32{1, 2, 3, 4, 5},
			Value:    3,
			Expected: 2,
		},
		{
			Data:     []int32{1, 2, 3, 4, 5},
			Value:    6,
			Expected: 5,
		},
		{
			Data:     []int32{1, 2, 3, 4, 5},
			Value:    0,
			Expected: 0,
		},
	}

	for _, d := range data {
		found := firstGreaterOrEqual(d.Data, d.Value)
		if found != d.Expected {
			t.Fatalf("data %+v value %d expected %d got %d", d.Data, d.Value, d.Expected, found)
		}
	}
}

func TestState_applyImpl(t *testing.T) {
	data := []struct {
		Diff     []*Diff
		Expected []int32
	}{
		{
			Diff: []*Diff{
				{
					Operation: DiffOperationCreate,
					Value:     1,
				},
				{
					Operation: DiffOperationCreate,
					Value:     2,
				},
				{
					Operation: DiffOperationCreate,
					Value:     3,
				},
				{
					Operation: DiffOperationCreate,
					Value:     4,
				},
				{
					Operation: DiffOperationCreate,
					Value:     5,
				},
			},
			Expected: []int32{1, 2, 3, 4, 5},
		},
		{
			Diff: []*Diff{
				{
					Operation: DiffOperationCreate,
					Value:     5,
				},
				{
					Operation: DiffOperationCreate,
					Value:     4,
				},
				{
					Operation: DiffOperationCreate,
					Value:     3,
				},
				{
					Operation: DiffOperationCreate,
					Value:     2,
				},
				{
					Operation: DiffOperationCreate,
					Value:     1,
				},
			},
			Expected: []int32{1, 2, 3, 4, 5},
		},
		{
			Diff: []*Diff{
				{
					Operation: DiffOperationCreate,
					Value:     2,
				},
				{
					Operation: DiffOperationCreate,
					Value:     3,
				},
				{
					Operation: DiffOperationCreate,
					Value:     1,
				},
			},
			Expected: []int32{1, 2, 3},
		},
		{
			Diff: []*Diff{
				{
					Operation: DiffOperationCreate,
					Value:     1,
				},
			},
			Expected: []int32{1},
		},
		{
			Diff: []*Diff{
				{
					Operation: DiffOperationCreate,
					Value:     4,
				},
				{
					Operation: DiffOperationCreate,
					Value:     3,
				},
				{
					Operation: DiffOperationCreate,
					Value:     2,
				},
				{
					Operation: DiffOperationCreate,
					Value:     5,
				},
				{
					Operation: DiffOperationCreate,
					Value:     1,
				},
				{
					Operation: DiffOperationDelete,
					Index:     2,
				},
				{
					Operation: DiffOperationUpdate,
					Index:     2,
					Value:     7,
				},
			},
			Expected: []int32{1, 2, 5, 7},
		},
		{
			Diff: []*Diff{
				{
					Operation: DiffOperationCreate,
					Value:     2,
				},
				{
					Operation: DiffOperationCreate,
					Value:     3,
				},
				{
					Operation: DiffOperationCreate,
					Value:     1,
				},
				{
					Operation: DiffOperationDelete,
					Index:     2,
				},
				{
					Operation: DiffOperationDelete,
					Index:     1,
				},
				{
					Operation: DiffOperationDelete,
					Index:     0,
				},
			},
			Expected: []int32{},
		},
		{
			Diff: []*Diff{
				{
					Operation: DiffOperationCreate,
					Value:     2,
				},
				{
					Operation: DiffOperationCreate,
					Value:     3,
				},
				{
					Operation: DiffOperationCreate,
					Value:     1,
				},
				{
					Operation: DiffOperationUpdate,
					Index:     2,
					Value:     -1,
				},
				{
					Operation: DiffOperationUpdate,
					Index:     2,
					Value:     -2,
				},
				{
					Operation: DiffOperationUpdate,
					Index:     2,
					Value:     -3,
				},
			},
			Expected: []int32{-3, -2, -1},
		},
		{
			Diff: []*Diff{
				{
					Operation: DiffOperationCreate,
					Value:     4,
				},
				{
					Operation: DiffOperationCreate,
					Value:     3,
				},
				{
					Operation: DiffOperationCreate,
					Value:     1,
				},
				{
					Operation: DiffOperationCreate,
					Value:     5,
				},
				{
					Operation: DiffOperationCreate,
					Value:     2,
				},
				{
					Operation: DiffOperationUpdate,
					Index:     4,
					Value:     0,
				},
				{
					Operation: DiffOperationDelete,
					Index:     4,
				},
			},
			Expected: []int32{0, 1, 2, 3},
		},
	}

	for ind, d := range data {
		func() {
			s := NewState()
			s.Start()
			defer s.Stop()

			for _, diff := range d.Diff {
				s.applyImpl(diff)
			}

			for i, v := range d.Expected {
				if actual, _ := s.Get(i); actual != v {
					if actual != v {
						t.Fatalf("test %d expected value %d at index %d got %d", ind, v, i, actual)
					}
				}
			}

			if !reflect.DeepEqual(s.data, d.Expected) {
				t.Fatalf("test %d expected array %+v got %+v", ind, d.Expected, s.data)
			}
		}()
	}
}
