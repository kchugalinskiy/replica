package main

import "sync"

type Connection interface {
	Apply(d *Diff)
}

type DummyServer struct {
	State *State
	Conns []Connection
	ch    chan *Diff
	m     sync.RWMutex
}

func NewServer() *DummyServer {
	return &DummyServer{
		State: NewState(),
	}
}

// InitState is intended for initial setup only
func (s *DummyServer) InitState(d []int32) {
	s.m.Lock()
	defer s.m.Unlock()

	if s.State != nil {
		s.State.InitState(d)
	}
}

func (s *DummyServer) Start() {
	s.m.Lock()
	defer s.m.Unlock()

	s.ch = make(chan *Diff, messageBufSize)

	go func() {
		for {
			d, ok := <-s.ch
			if !ok {
				return
			}

			s.State.Apply(d)

			for _, c := range s.Conns {
				c.Apply(d)
			}
		}
	}()

	s.State.Start()
}

func (s *DummyServer) Close() {
	s.m.Lock()
	defer s.m.Unlock()

	s.State.Stop()
	if s.ch != nil {
		close(s.ch)
		s.ch = nil
	}
}

// Apply is usually being invoked by http, we call it for simplification
func (s *DummyServer) Accept(c Connection) {
	s.m.Lock()
	defer s.m.Unlock()

	s.Conns = append(s.Conns, c)
}

// Disconnect is usually being invoked by http, we call it for simplification
func (s *DummyServer) Disconnect(c Connection) {
	s.m.Lock()
	defer s.m.Unlock()

	for i, conn := range s.Conns {
		if conn == c {
			if i+1 >= len(s.Conns) {
				s.Conns = s.Conns[0:i]
				return
			}
			s.Conns = append(s.Conns[0:i], s.Conns[i+1:len(s.Conns)]...)
		}
	}
}

// Apply is actually "receive" message from client handler
func (s *DummyServer) Apply(d *Diff) {
	s.m.RLock()
	defer s.m.RUnlock()

	// check for start/stop
	if s.ch != nil {
		// just for diff apply ordering
		s.ch <- d
	}
}

func (s *DummyServer) Clone() *State {
	s.m.RLock()
	defer s.m.RUnlock()

	return s.State.Clone()
}
