package main

type Server interface {
	Apply(d *Diff)
	Clone() *State
}

type DummyClient struct {
	State *State
	Srv   Server
}

func NewClient() *DummyClient {
	return &DummyClient{}
}

// Connect is ideally a http stack call, but for simplicity we use circular dependency
func (c *DummyClient) Connect(srv Server) {
	c.State = srv.Clone()
	c.State.Start()
	c.Srv = srv
}

func (c *DummyClient) Close() {
	c.State.Stop()
	c.State = nil
	c.Srv = nil
}

func (c *DummyClient) Get(i int) (int32, bool) {
	return c.State.Get(i)
}

func (c *DummyClient) Len() int {
	return c.State.Len()
}

func (c *DummyClient) Create(val int32) {
	c.Srv.Apply(&Diff{
		Operation: DiffOperationCreate,
		Value:     val,
	})
}

func (c *DummyClient) Update(val int32, i int) {
	c.Srv.Apply(&Diff{
		Operation: DiffOperationUpdate,
		Index:     i,
		Value:     val,
	})
}

func (c *DummyClient) Delete(i int) {
	c.Srv.Apply(&Diff{
		Operation: DiffOperationDelete,
		Index:     i,
	})
}

// Apply is not a part of customer interface, but a hack for network interaction callback
func (c *DummyClient) Apply(d *Diff) {
	if c.State != nil {
		c.State.Apply(d)
	}
}
